# Last FM App

#### English
Personal project using the MVVM pattern with the Last FM API data source.
#### Spanish
Proyecto personal utilizando la arquitectura MVVM y como fuente de los datos API Last FM.

## Requirements and dependencies


## API URL
https://www.last.fm/api

## Versions

| Version | Date Change | Description |
|----------|----------------|---------------|
|**1.0.0**|08/06/2021| List, Detail views and webview with Last FM data |


## Comentarios de desarrollo

### Arquitectura
La arquitectura utilizada es MVVM, con clases base para parte del modelo en común y una herencia, los View Controllers principales utilizan el BaseViewController.

Como es una App es pequeña, solo tiene 2 pantallas y la finalidad de este pequeño proyecto es demostrar mis habilidades, he creado pantallas con 3 formas posibles, el uso de Storyboards, Xibs y crearla pantalla completamente por código.

- **Listado y pantalla detalle** creada en Main.storyboard
- **Celda de la tabla** creada la vista en fichero **ListTVC.xib**
- **WebView** (creada totalmente por código) dentro de la clase **DetailVC.swift**

En una arquitectura MVVM la navegación se crea un coordinador y las pantallas pueden ser xibs o por código, para poder tener más de un programador trabajando en la misma pantalla. En cambio con un Storyboard esto no es posible o sería una limitación a la hora de gestionar conflictos en Git.

### Listados: 
Todos los apartados que son listados de información están paginados (Top Artists, Busqueda y Albums en horizontal), la cantidad de información por página se puede ajustar el limitNumber de la clase endpointsAPI.swift

### WebView: 
Se ejecuta tanto en el botón "More Details" en la pantalla de detalle, como si se toca un álbum en el listado inferior, mostrará la web del álbum. (esto último no estaba contemplado en los requisitos, pero teniendo los datos quise añadir esta funcionalidad y reutilizar el webview.

### Descarga de Imágenes: 
He implementado la descarga de imágenes con una caché, para no sobrecargar de peticiones al realizar scroll, además he añadido una pequeña animación al mostrarlas.

### Problema con las imágenes
La API no me estaba devolviendo imágenes incorrectas para las consultas de los artistas, he dedicado más tiempo por verificar y descartar no fuera mi implementación, adjunto un par de capturas junto a un pequeño lo de las URLs sacadas por consola.

Siempre me sale una estrella por imagen, en cambio las imágenes de los Álbumes sale correctamente.
 ![List View](https://gitlab.com/djadell.e/last_fm/-/raw/develop/GitLab%20files/ListView_screen_shot_web.png)  ![Detail View](https://gitlab.com/djadell.e/last_fm/-/raw/develop/GitLab%20files/DetailView_screen_shot_web.png)


### Puntos pendientes de desarrollo:
- Implementar la encriptación de datos sensibles, como guardar en el keychain o encriptar la key de la API.
- Se podría instalar dos pods, el Alamofire para utilizar el Network Reachability o otro equivalente, para verificar el estado de conexión antes de ejecutar las consultas a la API.
- Otro sería el ProgressHUD para mostrarlo en la carga del detalle, por si hay una mala conexión y tarda más no de la sensación de quedarse congelada la App.
