//
//  BaseViewController.swift
//  lastfm
//
//  Created by David Adell Echevarria on 06/06/2021.
//

import UIKit

class BaseViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    //MARK: - Alerts
    func showAler(title: String?, message: String?) {
        showAlert(title: title, message: title, actions: [UIAlertAction(title: "OK", style: .cancel, handler: nil)])
    }
    
    func showAlert(title: String?, message: String?, preferredStyle: UIAlertController.Style? = .alert, actions: [UIAlertAction]?) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: preferredStyle ?? .alert)
            if let actions = actions {
                for action in actions {
                    alert.addAction(action)
                }
            self.present(alert, animated: true)
        }
    }

}
