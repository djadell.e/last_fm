//
//  DBArtist.swift
//  lastfm
//
//  Created by David Adell Echevarria on 06/06/2021.
//

import Foundation

//MARK: - Model
struct DBArtist: Decodable {
    let mbid: String?
    let name: String?
    let image: [Image]?
    let playcount: String?
    let url: String?
    let bio: Bio?
}

//MARK: Image
struct Image: Decodable {
    var text: String
    var size: String
    private enum CodingKeys : String, CodingKey {
        case text = "#text"
        case size = "size"
    }
}

struct Bio: Decodable {
    let summary: String?
}


