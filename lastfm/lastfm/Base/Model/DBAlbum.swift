//
//  DBAlbum.swift
//  lastfm
//
//  Created by David Adell Echevarria on 08/06/2021.
//

import Foundation

struct DBAlbum: Decodable {
    let mbid: String?
    let name: String?
    let playcount: Int?
    let url: String?
    let artist: DBArtist?
    let image: [AlbumImage]?
}

//MARK: Image
struct AlbumImage: Decodable {
    var text: String
    var size: String
    private enum CodingKeys : String, CodingKey {
        case text = "#text"
        case size = "size"
    }
}


