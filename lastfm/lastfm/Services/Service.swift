//
//  Service.swift
//  lastfm
//
//  Created by David Adell Echevarria on 06/06/2021.
//

import UIKit

class Service: NSObject {
    static let shared = Service()
    var page: Int = 1
    var pageAlbum: Int = 1
    
    //MARK: - Public functions
    //MARK: Top Artists
    func fetchFirtsTopArtists(success: (@escaping ([DBArtist]) -> Void), failure: (@escaping (Error?)-> Void)) {
        page = page != 1 ? 1 : page
        getTopArtists(page: page, success: success, failure: failure)
    }
    
    func fetchNextTopArtists(success: (@escaping ([DBArtist]) -> Void), failure: (@escaping (Error?)-> Void)) {
        page+=1
        getTopArtists(page: page, success: success, failure: failure)
    }
    
    //MARK: Search Artists
    func fetchFirtsSearchArtist(name: String, success: (@escaping ([DBArtist]) -> Void), failure: (@escaping (Error?)-> Void)) {
        page = page != 1 ? 1 : page
        getSearchArtists(name: name, page: page, success: success, failure: failure)
    }
    
    func fetchNextSearchArtist(name: String, success: (@escaping ([DBArtist]) -> Void), failure: (@escaping (Error?)-> Void)) {
        page+=1
        getSearchArtists(name: name, page: page, success: success, failure: failure)
    }
    
    //MARK: Detail Artist
    func getDetailArtist(id: String, name: String, success: (@escaping (DBArtist?) -> Void), failure: (@escaping (Error?)-> Void)){
        getArtistDetails(id: id, name: name, success: success, failure: failure)
    }
    
    //MARK: Top Albums
    func fetchFirtsTopAlbums(id: String, name: String, success: (@escaping ([DBAlbum]) -> Void), failure: (@escaping (Error?)-> Void)) {
        pageAlbum = pageAlbum != 1 ? 1 : pageAlbum
        getTopAlbums(id: id, name: name, page: pageAlbum, success: success, failure: failure)
    }
    
    func fetchNextTopAlbums(id: String, name: String, success: (@escaping ([DBAlbum]) -> Void), failure: (@escaping (Error?)-> Void)) {
        pageAlbum+=1
        getTopAlbums(id: id, name: name, page: pageAlbum, success: success, failure: failure)
    }
    
    //MARK: Images
    func getImageWithUrl(urlString: String, success: (@escaping (UIImage) -> Void), failure: (@escaping (Error?)-> Void)) {
        guard let url = URL(string: urlString) else { return }
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            if let error = error {
                DispatchQueue.main.async {
                    failure(error)
                }
                print("[\(String(describing: Service.self))] Failed to get remote image", error)
                return
            }
            guard let data = data else { return }
            
            if let image = UIImage(data: data) {
                DispatchQueue.main.async {
                    success(image)
                }
            }
        }.resume()
    }
    
    //MARK: - Private functions
    //MARK: Top Artists
    fileprivate func getTopArtists(page: Int, success: (@escaping ([DBArtist]) -> Void), failure: (@escaping (Error?)-> Void)){
        let urlString = baseURL+chart_getTopArtists+APIParameter+APIKey+limitParameter+limitNumber+pageNum+"\(page)"+jsonFormat
        
        guard let url = URL(string: urlString) else { return }
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            if let error = error {
                DispatchQueue.main.async {
                    failure(error)
                }
                print("[\(String(describing: Service.self))] Failed to fetch Top Artists:", error)
                return
            }
            // Check response
            guard let data = data else { return }
            do {
                let artistData = try JSONDecoder().decode(DBTopArtists.self, from: data)
                DispatchQueue.main.async {
                    success(artistData.artists?.artist ?? [])
                }
            } catch let jsonError {
                DispatchQueue.main.async {
                    failure(jsonError)
                }
                print("[\(String(describing: Service.self))] Failed to decode: ", jsonError)
            }
        }.resume()
    }
    //MARK: Search Artists
    fileprivate func getSearchArtists(name: String, page: Int, success: (@escaping ([DBArtist]) -> Void), failure: (@escaping (Error?)-> Void)){
        guard let nameEncoded = name.addingPercentEncoding(withAllowedCharacters: .alphanumerics) else { return }
        let urlString = baseURL+artist_search+APIParameter+APIKey+limitParameter+limitNumber+pageNum+"\(page)"+artist_name+"\(nameEncoded)"+jsonFormat
        
        guard let url = URL(string: urlString) else { return }
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            if let error = error {
                DispatchQueue.main.async {
                    failure(error)
                }
                print("[\(String(describing: Service.self))] Failed to fetch Top Artists:", error)
                return
            }
            // Check response
            guard let data = data else { return }
            do {
                let artistData = try JSONDecoder().decode(DBSearchArtists.self, from: data)
                DispatchQueue.main.async {
                    success(artistData.results?.artistmatches?.artist ?? [])
                }
            } catch let jsonError {
                DispatchQueue.main.async {
                    failure(jsonError)
                }
                print("[\(String(describing: Service.self))] Failed to decode: ", jsonError)
            }
        }.resume()
    }
    //MARK: Artist Details
    fileprivate func getArtistDetails(id: String, name: String, success: (@escaping (DBArtist?) -> Void), failure: (@escaping (Error?)-> Void)){
        var urlString = ""
        if id.count > 0 {
            urlString = baseURL+artist_getInfo+APIParameter+APIKey+artist_id+"\(id)"+jsonFormat
        } else if name.count > 0 {
            guard let nameEncoded = name.addingPercentEncoding(withAllowedCharacters: .alphanumerics) else { return }
            urlString = baseURL+artist_getInfo+APIParameter+APIKey+artist_name+"\(nameEncoded)"+autocorrect+jsonFormat
        }
        guard let url = URL(string: urlString) else { return }
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            if let error = error {
                DispatchQueue.main.async {
                    failure(error)
                }
                print("[\(String(describing: Service.self))] Failed to fetch Top Artists:", error)
                return
            }
            // Check response
            guard let data = data else { return }
            do {
                let artistData = try JSONDecoder().decode(DBArtistDetail.self, from: data)
                DispatchQueue.main.async {
                    success(artistData.artist)
                }
            } catch let jsonError {
                DispatchQueue.main.async {
                    failure(jsonError)
                }
                print("[\(String(describing: Service.self))] Failed to decode: ", jsonError)
            }
        }.resume()
    }
    //MARK: Top Albums
    fileprivate func getTopAlbums(id: String, name: String, page: Int, success: (@escaping ([DBAlbum]) -> Void), failure: (@escaping (Error?)-> Void)){
        var urlString = ""
        if id.count > 0 {
            urlString = baseURL+artist_getTopAlbums+APIParameter+APIKey+limitParameter+limitNumber+pageNum+"\(page)"+artist_id+"\(id)"+jsonFormat
        } else if name.count > 0 {
            guard let nameEncoded = name.addingPercentEncoding(withAllowedCharacters: .alphanumerics) else { return }
            urlString = baseURL+artist_getTopAlbums+APIParameter+APIKey+limitParameter+limitNumber+pageNum+"\(page)"+artist_name+"\(nameEncoded)"+autocorrect+jsonFormat
        }
        guard let url = URL(string: urlString) else { return }
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            if let error = error {
                DispatchQueue.main.async {
                    failure(error)
                }
                print("[\(String(describing: Service.self))] Failed to fetch Top Artists:", error)
                return
            }
            // Check response
            guard let data = data else { return }
            do {
                let albumsData = try JSONDecoder().decode(DBTopAlbums.self, from: data)
                DispatchQueue.main.async {
                    success(albumsData.topalbums?.album ?? [])
                }
            } catch let jsonError {
                DispatchQueue.main.async {
                    failure(jsonError)
                }
                print("[\(String(describing: Service.self))] Failed to decode: ", jsonError)
            }
        }.resume()
    }
}
