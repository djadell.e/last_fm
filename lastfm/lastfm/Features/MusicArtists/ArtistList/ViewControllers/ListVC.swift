//
//  ListVC.swift
//  lastfm
//
//  Created by David Adell Echevarria on 06/06/2021.
//

import UIKit

class ListVC: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    fileprivate let searchController = UISearchController(searchResultsController: nil)
    fileprivate var searchBar: UISearchBar { return searchController.searchBar }
    fileprivate var searchBarText: String?
    
    fileprivate static let cellID = "ListTVC"
    fileprivate static let loadingCellID = "LoadingTVC"
    fileprivate var isLoadingList: Bool = false
    fileprivate var musicArtists = [ArtistViewModel]()
    fileprivate let titlePage = "Top Artists"
    fileprivate var actualPageLoaded = 1
    
    //MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configUI()
        fetchData()
    }
    
    func configUI() {
        title = titlePage
        setupTableView()
        configureSearchController()
    }
    
    func configureSearchController() {
        searchController.obscuresBackgroundDuringPresentation = false
        searchBar.showsCancelButton = true
        searchBar.text = ""
        searchBar.placeholder = "Enter your favorite artist name here"
        searchBar.delegate = self
        tableView.tableHeaderView = searchController.searchBar
        definesPresentationContext = true
    }
    
    func setupTableView() {
        tableView.register(UINib.init(nibName: String(describing: ListTVC.self), bundle: nil), forCellReuseIdentifier: ListVC.cellID)
        tableView.register(UINib.init(nibName: String(describing: LoadingTVC.self), bundle: nil), forCellReuseIdentifier: ListVC.loadingCellID)
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    //MARK: - Top Data
    fileprivate func fetchData() {
        actualPageLoaded = actualPageLoaded != 1 ? 1 : actualPageLoaded
        self.isLoadingList = true
        ListViewModel.shared.fetchFirtsTopArtists { (artists) in
            self.musicArtists = artists.map({return ArtistViewModel(artist: $0)})
            self.reloadData()
        } failure: { (error) in
            self.showAler(title: "Error", message: error?.localizedDescription)
            self.isLoadingList = false
            return
        }
    }
    
    fileprivate func fetchMoreData() {
        self.isLoadingList = true
        tableView.reloadSections(IndexSet(integer: 1), with: .none)
        ListViewModel.shared.fetchNextTopArtists { (artists) in
            self.musicArtists += artists.map({return ArtistViewModel(artist: $0)})
            self.actualPageLoaded += 1
            self.reloadData()
        } failure: { (error) in
            self.showAler(title: "Error", message: error?.localizedDescription)
            self.isLoadingList = false
            return
        }
    }
    
    //MARK: - Search Data
    fileprivate func fetchSearchData() {
        actualPageLoaded = actualPageLoaded != 1 ? 1 : actualPageLoaded
        self.isLoadingList = true
        guard let searchText = searchBarText else { return }
        ListViewModel.shared.fetchFirtsSearchArtist(name: searchText) { (artists) in
            self.musicArtists = artists.map({return ArtistViewModel(artist: $0)})
            self.reloadData()
        } failure: { (error) in
            self.showAler(title: "Error", message: error?.localizedDescription)
            self.isLoadingList = false
            return
        }
    }
    
    fileprivate func fetchSearchMoreData() {
        self.isLoadingList = true
        guard let searchText = searchBarText else { return }
        ListViewModel.shared.fetchNextSearchArtist(name: searchText) { (artists) in
            self.musicArtists += artists.map({return ArtistViewModel(artist: $0)})
            self.actualPageLoaded += 1
            self.reloadData()
        } failure: { (error) in
            self.showAler(title: "Error", message: error?.localizedDescription)
            self.isLoadingList = false
            return
        }
    }
    //MARK: Reload Data
    fileprivate func reloadData() {
        title = "\(titlePage) - Page Loaded: \(actualPageLoaded)"
        self.tableView.reloadData()
        self.isLoadingList = false
    }
}

//MARK: - UITableViewDataSource
extension ListVC: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return self.musicArtists.count
        } else if section == 1 {
            return 1
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cellCandidate = tableView.dequeueReusableCell(withIdentifier: ListVC.cellID, for: indexPath)
            guard let cell = cellCandidate as? ListTVC else {
                return UITableViewCell()
            }
            let artistData = self.musicArtists[indexPath.row]
            cell.artistViewModel = artistData
            return cell
        } else {
            let cellCandidate = tableView.dequeueReusableCell(withIdentifier: ListVC.loadingCellID, for: indexPath)
            guard let cell = cellCandidate as? LoadingTVC else {
                return UITableViewCell()
            }
            cell.spinner.startAnimating()
            return cell
        }
    }
}

//MARK: - UITableViewDelegate
extension ListVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        if let vc = storyBoard.instantiateViewController(identifier: "detailvc") as? DetailVC {
            let artistData = self.musicArtists[indexPath.row]
            vc.artistKeys = (id: artistData.id, name:artistData.name)
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if (((scrollView.contentOffset.y + scrollView.frame.size.height) > scrollView.contentSize.height ) && !isLoadingList){
            if let searchText = self.searchBarText, searchText.count > 0 {
                self.fetchSearchMoreData()
            } else {
                self.fetchMoreData()
            }
        }
    }
}

//MARK: - UISearchBarDelegate
extension ListVC: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if !self.isLoadingList {
            self.searchBarText = searchBar.text
            fetchSearchData()
        }
        if searchText.count == 0 {
            fetchData()
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.searchBarText = searchBar.text
        fetchSearchData()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.searchBarText = nil
        fetchData()
    }
}
