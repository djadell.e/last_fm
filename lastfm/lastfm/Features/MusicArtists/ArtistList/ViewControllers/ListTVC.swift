//
//  ListTVC.swift
//  lastfm
//
//  Created by David Adell Echevarria on 06/06/2021.
//

import UIKit

class ListTVC: UITableViewCell {
    
    @IBOutlet weak var posterImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var playcountLabel: UILabel!
    
    var artistViewModel: ArtistViewModel? {
        didSet {
            posterImageView.getImageWithUrl(urlString: artistViewModel?.image ?? "")
            titleLabel.text = artistViewModel?.name
            playcountLabel.text = artistViewModel?.playcount
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
