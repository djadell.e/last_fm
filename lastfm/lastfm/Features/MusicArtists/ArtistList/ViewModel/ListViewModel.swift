//
//  ListViewModel.swift
//  lastfm
//
//  Created by David Adell Echevarria on 06/06/2021.
//

import UIKit

class ListViewModel: NSObject {
    static let shared = ListViewModel()
    
    //MARK: - Get Top Artists
    func fetchFirtsTopArtists(success: (@escaping ([DBArtist]) -> Void), failure: (@escaping (Error?)-> Void)) {
        Service.shared.fetchFirtsTopArtists(success: success, failure: failure)
    }
    
    func fetchNextTopArtists(success: (@escaping ([DBArtist]) -> Void), failure: (@escaping (Error?)-> Void)) {
        Service.shared.fetchNextTopArtists(success: success, failure: failure)
    }
    
    //MARK: - Search Artists by name
    func fetchFirtsSearchArtist(name: String, success: (@escaping ([DBArtist]) -> Void), failure: (@escaping (Error?)-> Void)) {
        Service.shared.fetchFirtsSearchArtist(name: name, success: success, failure: failure)
    }
    
    func fetchNextSearchArtist(name: String, success: (@escaping ([DBArtist]) -> Void), failure: (@escaping (Error?)-> Void)) {
        Service.shared.fetchNextSearchArtist(name: name, success: success, failure: failure)
    }
}
