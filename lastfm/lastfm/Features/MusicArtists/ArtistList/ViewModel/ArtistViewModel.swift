//
//  ArtistViewModel.swift
//  lastfm
//
//  Created by David Adell Echevarria on 06/06/2021.
//

import Foundation

struct ArtistViewModel {
    let id: String
    let name: String
    let image: String
    let playcount: String
    let url: String
    
    init(artist: DBArtist) {
        self.id = artist.mbid ?? ""
        self.name = artist.name ?? "--"
        self.playcount = artist.playcount ?? ""
        self.url = artist.url ?? ""
        
        if let images = artist.image, let imageSelected = images.first(where: {$0.size == "medium"}) {
            self.image = imageSelected.text
        } else {
            self.image = ""
        }
    }
}


