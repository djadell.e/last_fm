//
//  DBTopArtists.swift
//  lastfm
//
//  Created by David Adell on 07/06/2021.
//

import Foundation

//MARK: - Model
struct DBTopArtists: Decodable {
    let artists: TopArtists?
}

struct TopArtists: Decodable {
    var artist: [DBArtist]?
}
