//
//  DBSearchArtists.swift
//  lastfm
//
//  Created by David Adell Echevarria on 07/06/2021.
//

import Foundation

struct DBSearchArtists: Decodable {
    let results: ArtistMatches?
}

struct ArtistMatches: Decodable {
    let artistmatches: Artist?
}

struct Artist: Decodable{
    var artist: [DBArtist]?
}


