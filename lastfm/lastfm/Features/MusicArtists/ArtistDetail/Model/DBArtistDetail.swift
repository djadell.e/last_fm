//
//  DBArtistDetail.swift
//  lastfm
//
//  Created by David Adell Echevarria on 07/06/2021.
//

import Foundation

struct DBArtistDetail: Decodable {
    let artist: DBArtist?
}

