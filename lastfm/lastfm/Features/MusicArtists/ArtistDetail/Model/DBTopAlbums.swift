//
//  DBTopAlbums.swift
//  lastfm
//
//  Created by David Adell Echevarria on 08/06/2021.
//

import Foundation

struct DBTopAlbums: Decodable {
    let topalbums: TopAlbums?
}

struct TopAlbums: Decodable {
    var album : [DBAlbum]?
}
