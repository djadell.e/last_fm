//
//  AlbumCVC.swift
//  lastfm
//
//  Created by David Adell Echevarria on 08/06/2021.
//

import UIKit

class AlbumCVC: UICollectionViewCell {
    
    @IBOutlet weak var albumImageView: UIImageView!
    
    var albumVM: AlbumViewModel? {
        didSet {
            albumImageView.getImageWithUrl(urlString: albumVM?.image ?? "")
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
