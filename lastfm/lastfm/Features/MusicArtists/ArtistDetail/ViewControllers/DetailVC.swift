//
//  DetailVC.swift
//  lastfm
//
//  Created by David Adell Echevarria on 06/06/2021.
//

import UIKit
import WebKit

class DetailVC: BaseViewController {
    
    @IBOutlet weak var artistImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var summaryTextView: UITextView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    fileprivate static let cellID = "AlbumCVC"
    fileprivate var albums = [AlbumViewModel]()
    fileprivate var isLoadingList: Bool = false
    
    var artistKeys = (id: "", name: "")
    fileprivate var artistDetailVM: ArtistDetailViewModel?
    
    //MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupCollectionView()
        getData()
        fetchFirtsTopAlbums()
    }
    
    func setupCollectionView() {
        collectionView.dataSource = self
        collectionView.delegate = self
    }
    
    //MARK: - Detail Data
    func configUI() {
        title = artistDetailVM?.name
        self.titleLabel.text = artistDetailVM?.name
        ///Image
        self.artistImageView.getImageWithUrl(urlString: artistDetailVM?.image ?? "")
        ///Summary
        self.summaryTextView.attributedText = artistDetailVM?.summary.htmlToAttributedString
    }
    
    func getData(){
        DetailViewModel.shared.getDetailArtist(id: artistKeys.id, name: artistKeys.name, success: { (artistDetails) in
            guard let artistDetails = artistDetails else {
                self.showAler(title: "Error", message:"No se ha podido recuperar la información de \(self.artistKeys.name)")
                return
            }
            self.artistDetailVM  = ArtistDetailViewModel(artist: artistDetails)
            self.configUI()
        }, failure: { (error) in
            self.showAler(title: "Error", message: error?.localizedDescription)
            return
        })
    }
    //MARK: - Albums Data
    func fetchFirtsTopAlbums(){
        self.isLoadingList = true
        DetailViewModel.shared.fetchFirtsTopAlbums(id: artistKeys.id, name: artistKeys.name) { (albums) in
            self.albums = albums.map({return AlbumViewModel(album: $0)})
            self.reloadData()
        } failure: { (error) in
            self.showAler(title: "Error", message: error?.localizedDescription)
            self.isLoadingList = false
            return
        }
    }
    
    func fetchNextTopAlbums(){
        self.isLoadingList = true
        DetailViewModel.shared.fetchNextTopAlbums(id: artistKeys.id, name: artistKeys.name) { (albums) in
            self.albums += albums.map({return AlbumViewModel(album: $0)})
            self.reloadData()
        } failure: { (error) in
            self.showAler(title: "Error", message: error?.localizedDescription)
            self.isLoadingList = false
            return
        }
    }
    
    //MARK: Reload Data
    fileprivate func reloadData() {
        self.collectionView.reloadData()
        self.isLoadingList = false
    }
    
    //MARK: - IBActions
    @IBAction func onMoreDetailsPressed(_ sender: Any) {
        openWebViewWithURL(url: artistDetailVM?.url)
    }
}

//MARK: -  WebView
/**
 * Development The Code Vista Controller without XIB file or Storyboard.
 */
extension DetailVC {
    func openWebViewWithURL(url: String?) {
        let vc = BaseViewController.init()
        //close buttom
        let closeButton = UIButton.init(type: .close)
        closeButton.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        closeButton.addTarget(self, action: #selector(closeWebview), for: .touchUpInside)
        //webView
        let webView = WKWebView()
        webView.frame  = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        guard let artistURL = url, let url = URL(string:artistURL) else { return }
        webView.load(URLRequest(url: url))
        //Add the components and show the webview
        vc.view.addSubview(webView)
        vc.view.addSubview(closeButton)
        present(vc, animated: true, completion: nil)
    }
    
    @objc func closeWebview(sender : UIButton) {
        dismiss(animated: true, completion: nil)
    }
}

//MARK: - UICollectionViewDataSource
extension DetailVC: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return albums.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cellCandidate = collectionView.dequeueReusableCell(withReuseIdentifier: DetailVC.cellID, for: indexPath)
        guard let cell = cellCandidate as? AlbumCVC else {
            return UICollectionViewCell()
        }
        cell.albumVM = albums[indexPath.row]
        return cell
    }
}

//MARK: - UICollectionViewDelegate
extension DetailVC: UICollectionViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if (((scrollView.contentOffset.x + scrollView.frame.size.width) > scrollView.contentSize.width ) && !isLoadingList){
            self.fetchNextTopAlbums()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        openWebViewWithURL(url: albums[indexPath.row].url)
    }
}
