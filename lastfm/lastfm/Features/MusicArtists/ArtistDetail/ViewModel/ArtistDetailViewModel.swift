//
//  ArtistDetailViewModel.swift
//  lastfm
//
//  Created by David Adell on 07/06/2021.
//

import Foundation

struct ArtistDetailViewModel {
    let id: String
    let name: String
    let image: String
    let summary: String
    let url: String
    
    init(artist: DBArtist) {
        self.id = artist.mbid ?? ""
        self.name = artist.name ?? "--"
        self.summary = artist.bio?.summary ?? ""
        self.url = artist.url ?? ""
        
        if let images = artist.image, let imageSelected = images.first(where: {$0.size == "large"}) {
            self.image = imageSelected.text
        } else {
            self.image = ""
        }
    }
}
