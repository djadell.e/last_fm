//
//  AlbumViewModel.swift
//  lastfm
//
//  Created by David Adell Echevarria on 08/06/2021.
//

import Foundation

struct AlbumViewModel {
    let id: String
    let name: String
    let image: String
    let url: String
    
    init(album: DBAlbum) {
        self.id = album.mbid ?? ""
        self.name = album.name ?? "--"
        self.url = album.url ?? ""
        
        if let images = album.image, let imageSelected = images.first(where: {$0.size == "large"}) {
            self.image = imageSelected.text
        } else {
            self.image = ""
        }
    }
}
