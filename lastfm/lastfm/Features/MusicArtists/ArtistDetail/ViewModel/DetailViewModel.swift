//
//  DetailViewModel.swift
//  lastfm
//
//  Created by David Adell Echevarria on 07/06/2021.
//

import UIKit

class DetailViewModel: NSObject {
    static let shared = DetailViewModel()
    
    //MARK: Detail Artist
    func getDetailArtist(id: String, name: String, success: (@escaping (DBArtist?) -> Void), failure: (@escaping (Error?)-> Void)){
        Service.shared.getDetailArtist(id: id, name: name, success: success, failure: failure)
    }
    
    //MARK: Top Albums
    func fetchFirtsTopAlbums(id: String, name: String, success: (@escaping ([DBAlbum]) -> Void), failure: (@escaping (Error?)-> Void)) {
        Service.shared.fetchFirtsTopAlbums(id: id, name: name, success: success, failure: failure)
    }
    
    func fetchNextTopAlbums(id: String, name: String, success: (@escaping ([DBAlbum]) -> Void), failure: (@escaping (Error?)-> Void)) {
        Service.shared.fetchNextTopAlbums(id: id, name: name, success: success, failure: failure)
    }
}
