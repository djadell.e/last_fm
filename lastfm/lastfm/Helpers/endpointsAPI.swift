//
//  endpointsAPI.swift
//  lastfm
//
//  Created by David Adell Echevarria on 06/06/2021.
//

import Foundation

//FIX

//MARK: - Base URL
let baseURL: String = "https://ws.audioscrobbler.com/2.0/"
//FIXME: Pending to save APIKey encriptyed or keychain!
let APIKey: String = "6102b8eff02a3a8ad7622a3216394dc0"
let APIParameter: String = "&api_key="
let jsonFormat: String = "&format=json"
let limitParameter: String = "&limit="
let limitNumber: String = "30"
let pageNum: String = "&page="
let autocorrect: String = "&autocorrect=1"

//MARK: - EndPoints

//MARK: Artist
// Info
let artist_getInfo: String = "?method=artist.getinfo"
let artist_name: String = "&artist="
let artist_id: String = "&mbid="

//Search
let artist_search: String = "?method=artist.search"

//Top Albums
let artist_getTopAlbums: String = "?method=artist.gettopalbums"

//MARK: Chart
//Top Artists
let chart_getTopArtists: String = "?method=chart.gettopartists"
